import { createApp } from 'vue'
import App from './App.vue'
import Home from './Home.vue'
import Contatti from './Contatti.vue'
import AutoList from './AutoList.vue'
import InfoAuto from './InfoAuto.vue'
import { createRouter, createWebHistory } from 'vue-router';



// definiamo tutte le rotte (URL) della nostra applicazione 
// ed i relativi componenti da montare
const routes = [
    { path: '/', component: Home },
    { path: '/contatti', component: Contatti },
    { path: '/auto-list', component: AutoList },
    { path: '/info-auto?id=:id', name: 'auto', component: InfoAuto },
]
 
// creo l'istanza router e gli passo le rotte
const router = createRouter({
    // usa la web history
    history: createWebHistory(),
    routes: routes, // short for `routes: routes`
})
 
// creo l'applicazione Vue
const app = createApp(App);
// gli dico di usare il router
app.use(router);
// monto l'applicazione sul tag #app
app.mount('#app');