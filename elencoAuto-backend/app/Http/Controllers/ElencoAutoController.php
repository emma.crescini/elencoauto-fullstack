<?php

namespace App\Http\Controllers;

/** serve per accedere all'oggeto request che contiene i dati forniti dal client */
use Illuminate\Http\Request;
/** serve per personalizzare il tipo di risposta HTTP */
use Illuminate\Http\Response;

class ElencoAutoController extends Controller
{
    /**
     * Questo controller gestisce la tabella todos
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    
    public function list()
    {
        $results = app('db')->select("SELECT * FROM elencoauto");
        return $results;
    }

    public function listAuto($id)
    {
        $results = app('db')->select("SELECT * FROM elencoauto WHERE idAuto=$id");
        return $results;
    }


    
     public function create(Request $request)
     {
        $marca = $request->input('marca');
        $modello = $request->input('modello');
        $targa = $request->input('targa'); //null si
        $dataImm = $request->input('dataImm'); //null si
        $immagine = $request->input('immagine');
        $prezzo = $request->input('prezzo');
        $descrizione = $request->input('descrizione');
        $video = $request->input('video');

        if($targa && $dataImm){
            $results = app('db')->insert("
                INSERT INTO `elencoauto` (`marca`, `modello`, `targa`, `dataImm`, `immagine`, `video`, `prezzo`, `descrizione`) 
                VALUES ('$marca', '$modello', '$targa', '$dataImm', '$immagine', '$video', '$prezzo', '$descrizione');
            ");
        }else if($targa && !$dataImm){
            $results = app('db')->insert("
                INSERT INTO `elencoauto` (`marca`, `modello`, `targa`, `immagine`, `video`, `prezzo`, `descrizione`) 
                VALUES ('$marca', '$modello','$targa', '$immagine', '$video', '$prezzo', '$descrizione');
            ");
        }else if(!$targa && $dataImm){
            $results = app('db')->insert("
                INSERT INTO `elencoauto` (`marca`, `modello`, `dataImm`, `immagine`, `video`, `prezzo`, `descrizione`) 
                VALUES ('$marca', '$modello', '$dataImm', $immagine, '$video', '$prezzo', '$descrizione');
            ");
        }else if(!$targa && !$dataImm){
            $results = app('db')->insert("
                INSERT INTO `elencoauto` (`marca`, `modello`,`immagine`, `immagine`, `video`, `prezzo`, `descrizione`) 
                VALUES ('$marca', '$modello', '$immagine', '$video', '$prezzo', '$descrizione');
            ");
        }
        
        // ritorno una risposta http
        return new Response(null, 200);
     }


    public function update(Request $request,$id)
    {
        $marca = $request->input('marca');
        $modello = $request->input('modello');
        $targa = $request->input('targa'); //null si
        $dataImm = $request->input('dataImm'); //null si
        $immagine = $request->input('immagine');
        $prezzo = $request->input('prezzo');
        $descrizione = $request->input('descrizione');
        $video = $request->input('video');

        if($targa && $dataImm){
            $results = app('db')->update("
                UPDATE `elencoauto`
                SET 
                    `marca` = '$marca',
                    `modello` = '$modello',
                    `targa` = '$targa',
                    `dataImm` = '$dataImm',
                    `immagine` = '$immagine',
                    `video` = '$video',
                    `prezzo` = '$prezzo',
                    `descrizione` = '$descrizione'
                WHERE 
                    `elencoAuto`.`idAuto` = $id;
            ");
        }else if($targa && !$dataImm){
            $results = app('db')->update("
                UPDATE `elencoauto`
                SET 
                    `marca` = '$marca',
                    `modello` = '$modello',
                    `targa` = '$targa',
                    `immagine` = '$immagine',
                    `video` = '$video',
                    `prezzo` = '$prezzo',
                    `descrizione` = '$descrizione'
                WHERE 
                    `elencoAuto`.`idAuto` = $id;
            ");
        }else if(!$targa && $dataImm){
            $results = app('db')->update("
                UPDATE `elencoauto`
                SET 
                    `marca` = '$marca',
                    `modello` = '$modello',
                    `dataImm` = '$dataImm',
                    `immagine` = '$immagine',
                    `video` = '$video',
                    `prezzo` = '$prezzo',
                    `descrizione` = '$descrizione'
                WHERE 
                    `elencoAuto`.`idAuto` = $id;
            ");
        }else if(!$targa && !$dataImm){
            $results = app('db')->update("
                UPDATE `elencoauto`
                SET 
                    `marca` = '$marca',
                    `modello` = '$modello',
                    `immagine` = '$immagine',
                    `video` = '$video',
                    `prezzo` = '$prezzo',
                    `descrizione` = '$descrizione'
                WHERE 
                    `elencoAuto`.`idAuto` = $id;
            ");
        }
       // ritorno una risposta http
       return new Response(null, 200);
    }

    public function delete(Request $request,$id)
    {
        $results = app('db')->delete("
            DELETE FROM elencoauto WHERE idAuto = $id
        ");
       // ritorno una risposta http
       return new Response(null, 204);
    }
}
